<?php
defined('ABSPATH') || die;

class EsvApi {

	private static $baseUrl = 'https://api.esv.org/v3';
	private static $apiKey = false;

	private static $instance = null;
	private function __construct() { }

	private static function request ( $url, $params=[], $jsonDecode=true ) {

		$apikey = self::getApiKey();
		if( ! $apikey )
			return new WP_Error('api-error', 'No API Key set for the ESV api');

		if( $params ){
			$url = rtrim($url, '?');
			$url .= '?' . http_build_query($params);
		}
		// error_log($url);
		$results = wp_remote_get( $url, [
			'headers' => array(
				'Authorization' => "Token $apikey",
			),
		]);

		$result_body = wp_remote_retrieve_body($results);
		if( $jsonDecode )
			$result_body = json_decode($result_body);
		return $result_body;
	}

	public static function get_instance() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}

	static function getApiKey () {
		$default = false;
		return get_option('esv_api3_key', $default);
	}
	static function setApiKey ($key) {
		update_option('esv_api3_key', $key);
	}

	static function getVerses ($lookup, $returnType='html', $overrideParams=[]) {
		$url = self::$baseUrl . '/passage';

		$params = array_merge([
			'include-passage-references' => "false",
			'include-footnotes' => "false",
			'include-css-link' => "false",
		], $overrideParams);

		$params['q'] = $lookup;

		switch($returnType) {
			case 'html' :
				$url .= '/html/';
				break;
			default :
				$url .= '/text/';
				break;
		}
		$resp = self::request( $url, $params);

		if(  is_wp_error( $resp ) )
			return $resp->get_error_message();

		if( ! $resp || ! property_exists($resp, 'passages') ) {
			// error_log(json_encode($resp));
			return false;
		}

		return array_shift( $resp->passages );
	}

	static function searchBible ($searchText, $overrideParams=[]) {

		if( empty($searchText) )
			return false;

		$url = self::$baseUrl . '/passage/search/';
		$params = array_merge([
			'page-size' => 10,
			'page' => 1,
		], $overrideParams);
		
		$params['q'] = $searchText;

		$resp = self::request( $url, $params );

		return $resp->results;
	}

	static function getAudio ( $lookup ) {
		$url = self::$baseUrl . '/passage/audio/';

		// This returns a redirect to the mp3 file. 
		// @XXX untested
		return self::request( $url, [
			'q' => $lookup,
		], false );
	}



}
