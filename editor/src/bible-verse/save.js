
import {
	useBlockProps,
} from '@wordpress/block-editor';

import { 
	parseBibleBook, 
} from './lib';

export default function save( props ) {

	const blockProps = useBlockProps.save({
		className: 'single-sermon-texts single-sermon-content-column',
	});

	const { attributes } = props;

	const passage = attributes.passageClone;

	const bibleBook = parseBibleBook( passage );

	const otherSermonsLink = (bibleBook && attributes.displayLinkToBookTaxonomy) ? (
		<p className="sermon-other-from-book">
			<a href={ attributes.bookLink }>Other sermons from { bibleBook }</a>
		</p>
	) : false;

	return (
		<section {...blockProps}>
			
			<h3 className="sermon-passage">{ passage }</h3>

			{ otherSermonsLink }
			
			<div className="sermon-bible-text" 
				dangerouslySetInnerHTML={{
					__html: attributes.bibleText,
				}}
			/>

		</section>
	);
}
	