
import edit from './edit';
import save from './save';
import BibleIcon from '../BibleIcon';

import blockData from './block.json';

import {
	registerBlockType,
} from '@wordpress/blocks';

registerBlockType( blockData.name, {
	...blockData,
	edit,
	save,
	icon: BibleIcon,
});
