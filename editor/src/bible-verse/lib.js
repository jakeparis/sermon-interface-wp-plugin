

const parseBibleBook = function(passage) {
	if( ! passage ) return false;
	var book = passage.match(/^([123]? ?[a-zA-Z ]+)/i)
	if( ! book )
		return false;
	return book[0].trim();
};


export { parseBibleBook }
