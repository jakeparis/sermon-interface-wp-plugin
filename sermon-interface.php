<?php
/*
Plugin Name: Sermon Manager and Interface
Description: Adds support for sermons, including a search interface
Version: 4.0.6
Requires at least: 5.6
Requires PHP: 7.4
Author: Jake Paris
Author URI: https://jakeparis.com
*/

define('SERMON_MANAGER_INTERFACE_VERSION', '4.0.6');
define('SERMON_MANAGER_INTERFACE_PATH', plugin_dir_path(__FILE__) );
define('SERMON_MANAGER_INTERFACE_URL', plugins_url('/', __FILE__) );

require_once SERMON_MANAGER_INTERFACE_PATH . 'version_updates.php';
require_once SERMON_MANAGER_INTERFACE_PATH . 'sermonCPT.php';
require_once SERMON_MANAGER_INTERFACE_PATH . 'class/SermonsManager.class.php';
require_once SERMON_MANAGER_INTERFACE_PATH . 'class/Sermon.class.php';
require_once SERMON_MANAGER_INTERFACE_PATH . 'widgets.php';
require_once SERMON_MANAGER_INTERFACE_PATH . 'editor/editor.php';
require_once SERMON_MANAGER_INTERFACE_PATH . 'rest.php';


add_action('admin_menu', function(){
	add_options_page('Sermon Manager','Sermon Manager','manage_options','sermon-manager', 'jpsmi_settingsPage');
});
function jpsmi_settingsPage() {
	require_once SERMON_MANAGER_INTERFACE_PATH . 'admin-settings-page.php';
}

add_action('wp_enqueue_scripts', function(){
	wp_enqueue_style( 
		'sermon-interface', 
		plugins_url('public-assets/build/main.css',__FILE__), 
		array(), 
		SERMON_MANAGER_INTERFACE_VERSION
	);
	wp_enqueue_script(
		'sermon-interface', 
		plugins_url('public-assets/build/main.js',__FILE__), 
		array('jquery'), 
		SERMON_MANAGER_INTERFACE_VERSION
	);
}, 80);

// This is to ensure that siteurl still shows when in multisite
add_action('init', function(){
	register_setting(
	    'general',
	    'siteurl',
	    array(
	        'show_in_rest' => array(
	            'name'   => 'url',
	            'schema' => array(
	                'format' => 'uri',
	            ),
	        ),
	        'type'         => 'string',
	        'description'  => 'Site URL.',
	    )
	);
});

add_filter('wp_headers', function($headers){

	if( ! isset($_GET['sermon-file-download']) )
		return $headers;

	$attid = (int) $_GET['sermon-file-download'];

	if( ! wp_attachment_is('audio',$attid) )
		wp_die('Sorry, that file cannot be downloaded this way (2).');

	$url = wp_get_attachment_url($attid);

	if($url===false)
		wp_die('Sorry, that file cannot be downloaded this way (3).');

	$mime = get_post_mime_type($attid);
	$filename = basename($url);

	header("Content-disposition: attachment; filename={$filename}");
	header("Content-type: $mime");
	header('Content-Description: File Transfer');
	readfile($url);
	exit;
});


add_action('wp', function(){
	add_shortcode( 'latest-sermon', array('SermonsManager','getLatestSermonBlock') );
	// alias
	add_shortcode( 'most-recent-sermon', array('SermonsManager','getLatestSermonBlock') );
	add_shortcode('sermons', array('SermonsManager', 'shortcode_doSermonPage') );
	add_shortcode('display-sermon', array('SermonsManager', 'shortcode_printSingleSermon') );
});



/**
 * Updater
 */
require SERMON_MANAGER_INTERFACE_PATH . 'updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/wp-plugin-sermon-interface/',
	__FILE__, //Full path to the main plugin file or functions.php.
	'sermon-interface/sermon-interface.php'
);
// $myUpdateChecker->setAuthentication($defaultToken);


register_deactivation_hook( __FILE__, function(){
	delete_option('sermon_manager_interface_version');
});
