<?php
defined('ABSPATH') || die('Not allowed');


class Latest_Sermon_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('latest-sermon-widget', 'Latest Sermon');
	}

	function widget( $args, $instance ) {
		echo $args['before_widget'];
		
		echo $args['before_title'];
		echo $instance['title'];
		echo $args['after_title'];

		$args = array();
		if( $instance['withVideo'] == '1' )
			$args['with-video'] = true;

		echo SermonsManager::getLatestSermonBlock($args);

		echo $args['after_widget'];
	}


	function update( $new_instance, $old_instance ) {
		$updated_instance = $new_instance;
		return $updated_instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			'title' => '',
			'withVideo' => '0',
		));

		$id = $this->get_field_id('title');
		$name = $this->get_field_name('title');
		$val = $instance['title'];

		$withVideoId = $this->get_field_id('withVideo');
		$withVideoName = $this->get_field_name('withVideo');
		$withVideoChecked = $instance['withVideo']=='1' ? 'checked' : '';


		echo <<<OUT
		<label for="{$id}">Widget Title</label>
		<input type="text" value="{$val}" name="{$name}" id="{$id}" >

		<p>
			<input type="checkbox" value="1" name="{$withVideoName}" id="{$withVideoId}"
				{$withVideoChecked}>
			<label for="{$withVideoId}">Display Video (if any)?</label>
		</p>
OUT;
	}
}


add_action( 'widgets_init', function(){
	register_widget('Latest_Sermon_Widget');
});