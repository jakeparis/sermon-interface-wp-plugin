import './main.scss';

document.addEventListener("DOMContentLoaded", function() {
   
   var autoSubmitSelects = document.querySelectorAll('select.js-autosubmit');
   if( autoSubmitSelects ) {
      autoSubmitSelects.forEach( sel => {
         sel.addEventListener('change', e => {
            e.target.closest('form').submit();
         });
      })
   }

   var shortListings = document.querySelectorAll('.sermon-single-short-listing');
   if( shortListings ) {
      shortListings.forEach( sl => {
         sl.addEventListener('click', function(e) {
            if( e.target.nodeName === 'A' )
               return;
            let url = this.querySelector('.sermon-link a').getAttribute('href');
            if( url )
               window.location = url;
         })
      });
   }

});
