jQuery(function($){
	var handleReturnedData = function(txt){
		var j = JSON.parse( txt );

		if( ! j.data )
			return;

		// if we are dealing with a response from an uploaded file
		if( typeof j.data.uploadedToLink != 'undefined' && ['audio/mpeg','audio/ogg','audio/x-mpeg-3','audio/wav','audio/x-ms-wma'].indexOf(j.data.mime) > -1 )
		{
			// console.log(j.data);
			var attId = j.data.id
			// var mimetype = j.mime;
			// var subtype = j.subtype; // (e.g. "png")
			var deleteLink = window.location.origin + window.location.pathname;

			if(window.location.search.indexOf('removeAttachmentId') > -1){
				deleteLink += window.location.search.replace(/(removeAttachmentId=)[0-9]+/, '$1' + attId);
			} else {
				deleteLink += window.location.search + '&removeAttachmentId=' + attId;
			}

			var html = `
				<p class="sermon-interface-media-temp-inline-msg"><b>Attached: <a target="_blank" href="${j.data.url}">${j.data.filename}</a></b> <span style="color:gray;">(update post to manage)</span></td>
				</p>
			`;
			$('.sound-files-list').html(html);

			$('.sermon-interface-media-upload-msg').remove();
			$('.media-frame-toolbar .media-button-insert').hide()
				.after('<p class="sermon-interface-media-upload-msg">Your file is now attached. You can close this popup.</p>');
			$('.media-modal-close').trigger('click');

		} else { // other uploads (images)
			if( typeof typenow !== 'undefined' && typenow === 'sermon') {
				$('.sermon-interface-media-upload-msg').remove();
				$('.media-frame-toolbar .media-button-insert').hide()
						.after('<p class="sermon-interface-media-upload-msg">Copy the file url and manually create an &lt;img&gt; tag.</p>');
					}
		}
	};

	// the upload async is using vanilla ajax, so we can't grab the ajaxed data
	// with jquery's global ajax events. Here we overwrite the xmlhttprequest method,
	// only so that we can insert something to grab the data which comes down and have
	// our way with it.
	// @thanks  http://stackoverflow.com/a/26447781/362769
	(function() {
		var open = XMLHttpRequest.prototype.open;

		XMLHttpRequest.prototype.open = function(method, url, async, user, password) {
			var oldReady;
			if (async) {
				oldReady = this.onreadystatechange;
				// override onReadyStateChange
				this.onreadystatechange = function() {
					if (this.readyState == 4) {
						var self = this;
						// this is where we grab the data
						handleReturnedData( this.response );

						if(oldReady !== null)
							return oldReady.call(self);
					} else {
						// call original onreadystatechange handler
						if(oldReady !== null)
							return oldReady.apply(this, arguments);
					}
				}
			}
			// call original open method
			return open.apply(this, arguments);
		};

	})();

	$('#js-insert-last-sunday').on('click',function(e){
		e.preventDefault();
		var d = $(this).data('value');
		$('input[name="sermon-date"]').val(d);
	});

});
