<?php
defined('ABSPATH') || die('Not allowed');

require_once SERMON_MANAGER_INTERFACE_PATH . 'class/esv-api.class.php';
?>
<style>
	label.block {
		font-weight: bold;
		margin: .8em 0 .2em;
	}
</style>
	
<div class="wrap sermons-settings-page">

<?php

if( isset($_POST['save-sermons-settings']) ) {
	if( ! wp_verify_nonce( $_POST['sermon-settings-nonce'], '_save_sermon_settings' ) ) {
		echo '<div class="updated error"><p>There was an error saving. Try again. </p></div>';
	} else {

		$esv = EsvApi::get_instance();
		$esv->setApiKey( $_POST['esv_api3_key'] );

		echo '<div class="updated"><p>Saved settings</p></div>';

	}
}

$esvApi3Key = EsvApi::getApiKey();

?>

	<h1>Sermons Manager</h1>
	<form method="post">
	
	<?php wp_nonce_field( '_save_sermon_settings', 'sermon-settings-nonce' ) ?>

	<label for="esv_api3_key" class="block">ESV API Key</label>
	<p>Enter your api key to display bible texts along with sermons. If you don't have one, you can get one from <a href="https://api.esv.org/account/create-application/">https://api.esv.org/account/create-application/</a>
	</p>
	<input type="text" name="esv_api3_key" id="esv_api3_key" value="<?= $esvApi3Key ?>">

	<?php submit_button( "Save", 'primary', 'save-sermons-settings' ) ?>
	</form>

</div>
